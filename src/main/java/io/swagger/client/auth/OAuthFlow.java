/*
 * Emergency RAVE controller
 * REST API for the Emergency RAVE controller. See https://emergency-rave.gitlab.io/about-erave/ for more information.
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}
