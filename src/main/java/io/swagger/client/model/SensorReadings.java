/*
 * Emergency RAVE controller
 * REST API for the Emergency RAVE controller. See https://emergency-rave.gitlab.io/about-erave/ for more information.
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * Sensor readings and valve status at a given time
 */
@Schema(description = "Sensor readings and valve status at a given time")
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-04-18T14:46:04.929Z[GMT]")
public class SensorReadings {
  @SerializedName("timestamp")
  private Integer timestamp = null;

  @SerializedName("pressureMixChamber")
  private Integer pressureMixChamber = null;

  @SerializedName("airFlow")
  private Double airFlow = null;

  @SerializedName("pressureLung")
  private Integer pressureLung = null;

  @SerializedName("valveStatusIntervalSec")
  private Double valveStatusIntervalSec = null;

  @SerializedName("compressorValveOpenRatio")
  private Double compressorValveOpenRatio = null;

  @SerializedName("oxygenValveOpenRatio")
  private Double oxygenValveOpenRatio = null;

  @SerializedName("inspirationValveOpenRatio")
  private Double inspirationValveOpenRatio = null;

  @SerializedName("expirationValveOpenRatio")
  private Double expirationValveOpenRatio = null;

  public SensorReadings timestamp(Integer timestamp) {
    this.timestamp = timestamp;
    return this;
  }

   /**
   * Get timestamp
   * @return timestamp
  **/
  @Schema(description = "")
  public Integer getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Integer timestamp) {
    this.timestamp = timestamp;
  }

  public SensorReadings pressureMixChamber(Integer pressureMixChamber) {
    this.pressureMixChamber = pressureMixChamber;
    return this;
  }

   /**
   * Get pressureMixChamber
   * @return pressureMixChamber
  **/
  @Schema(description = "")
  public Integer getPressureMixChamber() {
    return pressureMixChamber;
  }

  public void setPressureMixChamber(Integer pressureMixChamber) {
    this.pressureMixChamber = pressureMixChamber;
  }

  public SensorReadings airFlow(Double airFlow) {
    this.airFlow = airFlow;
    return this;
  }

   /**
   * Get airFlow
   * @return airFlow
  **/
  @Schema(description = "")
  public Double getAirFlow() {
    return airFlow;
  }

  public void setAirFlow(Double airFlow) {
    this.airFlow = airFlow;
  }

  public SensorReadings pressureLung(Integer pressureLung) {
    this.pressureLung = pressureLung;
    return this;
  }

   /**
   * Get pressureLung
   * @return pressureLung
  **/
  @Schema(description = "")
  public Integer getPressureLung() {
    return pressureLung;
  }

  public void setPressureLung(Integer pressureLung) {
    this.pressureLung = pressureLung;
  }

  public SensorReadings valveStatusIntervalSec(Double valveStatusIntervalSec) {
    this.valveStatusIntervalSec = valveStatusIntervalSec;
    return this;
  }

   /**
   * Get valveStatusIntervalSec
   * @return valveStatusIntervalSec
  **/
  @Schema(description = "")
  public Double getValveStatusIntervalSec() {
    return valveStatusIntervalSec;
  }

  public void setValveStatusIntervalSec(Double valveStatusIntervalSec) {
    this.valveStatusIntervalSec = valveStatusIntervalSec;
  }

  public SensorReadings compressorValveOpenRatio(Double compressorValveOpenRatio) {
    this.compressorValveOpenRatio = compressorValveOpenRatio;
    return this;
  }

   /**
   * Get compressorValveOpenRatio
   * @return compressorValveOpenRatio
  **/
  @Schema(description = "")
  public Double getCompressorValveOpenRatio() {
    return compressorValveOpenRatio;
  }

  public void setCompressorValveOpenRatio(Double compressorValveOpenRatio) {
    this.compressorValveOpenRatio = compressorValveOpenRatio;
  }

  public SensorReadings oxygenValveOpenRatio(Double oxygenValveOpenRatio) {
    this.oxygenValveOpenRatio = oxygenValveOpenRatio;
    return this;
  }

   /**
   * Get oxygenValveOpenRatio
   * @return oxygenValveOpenRatio
  **/
  @Schema(description = "")
  public Double getOxygenValveOpenRatio() {
    return oxygenValveOpenRatio;
  }

  public void setOxygenValveOpenRatio(Double oxygenValveOpenRatio) {
    this.oxygenValveOpenRatio = oxygenValveOpenRatio;
  }

  public SensorReadings inspirationValveOpenRatio(Double inspirationValveOpenRatio) {
    this.inspirationValveOpenRatio = inspirationValveOpenRatio;
    return this;
  }

   /**
   * Get inspirationValveOpenRatio
   * @return inspirationValveOpenRatio
  **/
  @Schema(description = "")
  public Double getInspirationValveOpenRatio() {
    return inspirationValveOpenRatio;
  }

  public void setInspirationValveOpenRatio(Double inspirationValveOpenRatio) {
    this.inspirationValveOpenRatio = inspirationValveOpenRatio;
  }

  public SensorReadings expirationValveOpenRatio(Double expirationValveOpenRatio) {
    this.expirationValveOpenRatio = expirationValveOpenRatio;
    return this;
  }

   /**
   * Get expirationValveOpenRatio
   * @return expirationValveOpenRatio
  **/
  @Schema(description = "")
  public Double getExpirationValveOpenRatio() {
    return expirationValveOpenRatio;
  }

  public void setExpirationValveOpenRatio(Double expirationValveOpenRatio) {
    this.expirationValveOpenRatio = expirationValveOpenRatio;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SensorReadings sensorReadings = (SensorReadings) o;
    return Objects.equals(this.timestamp, sensorReadings.timestamp) &&
        Objects.equals(this.pressureMixChamber, sensorReadings.pressureMixChamber) &&
        Objects.equals(this.airFlow, sensorReadings.airFlow) &&
        Objects.equals(this.pressureLung, sensorReadings.pressureLung) &&
        Objects.equals(this.valveStatusIntervalSec, sensorReadings.valveStatusIntervalSec) &&
        Objects.equals(this.compressorValveOpenRatio, sensorReadings.compressorValveOpenRatio) &&
        Objects.equals(this.oxygenValveOpenRatio, sensorReadings.oxygenValveOpenRatio) &&
        Objects.equals(this.inspirationValveOpenRatio, sensorReadings.inspirationValveOpenRatio) &&
        Objects.equals(this.expirationValveOpenRatio, sensorReadings.expirationValveOpenRatio);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, pressureMixChamber, airFlow, pressureLung, valveStatusIntervalSec, compressorValveOpenRatio, oxygenValveOpenRatio, inspirationValveOpenRatio, expirationValveOpenRatio);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SensorReadings {\n");
    
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    pressureMixChamber: ").append(toIndentedString(pressureMixChamber)).append("\n");
    sb.append("    airFlow: ").append(toIndentedString(airFlow)).append("\n");
    sb.append("    pressureLung: ").append(toIndentedString(pressureLung)).append("\n");
    sb.append("    valveStatusIntervalSec: ").append(toIndentedString(valveStatusIntervalSec)).append("\n");
    sb.append("    compressorValveOpenRatio: ").append(toIndentedString(compressorValveOpenRatio)).append("\n");
    sb.append("    oxygenValveOpenRatio: ").append(toIndentedString(oxygenValveOpenRatio)).append("\n");
    sb.append("    inspirationValveOpenRatio: ").append(toIndentedString(inspirationValveOpenRatio)).append("\n");
    sb.append("    expirationValveOpenRatio: ").append(toIndentedString(expirationValveOpenRatio)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
