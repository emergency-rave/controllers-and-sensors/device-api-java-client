/*
 * Emergency RAVE controller
 * REST API for the Emergency RAVE controller. See https://emergency-rave.gitlab.io/about-erave/ for more information.
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
/**
 * Runtime parameters set by the caregiver
 */
@Schema(description = "Runtime parameters set by the caregiver")
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-04-18T14:46:04.929Z[GMT]")
public class TreatmentParameters {
  @SerializedName("pressure")
  private Float pressure = null;

  @SerializedName("frequency")
  private Float frequency = null;

  @SerializedName("flow")
  private Float flow = null;

  @SerializedName("lung-tidal-vol")
  private Float lungTidalVol = null;

  @SerializedName("lung-inspiratory-reserve-vol")
  private Float lungInspiratoryReserveVol = null;

  @SerializedName("lung-expiratory-reserve-vol")
  private Float lungExpiratoryReserveVol = null;

  @SerializedName("lung-residual-vol")
  private Float lungResidualVol = null;

  public TreatmentParameters pressure(Float pressure) {
    this.pressure = pressure;
    return this;
  }

   /**
   * required lung pressure in mbar
   * @return pressure
  **/
  @Schema(description = "required lung pressure in mbar")
  public Float getPressure() {
    return pressure;
  }

  public void setPressure(Float pressure) {
    this.pressure = pressure;
  }

  public TreatmentParameters frequency(Float frequency) {
    this.frequency = frequency;
    return this;
  }

   /**
   * breath cycles per second
   * @return frequency
  **/
  @Schema(description = "breath cycles per second")
  public Float getFrequency() {
    return frequency;
  }

  public void setFrequency(Float frequency) {
    this.frequency = frequency;
  }

  public TreatmentParameters flow(Float flow) {
    this.flow = flow;
    return this;
  }

   /**
   * moved gas volume per breath cycle in ml
   * @return flow
  **/
  @Schema(description = "moved gas volume per breath cycle in ml")
  public Float getFlow() {
    return flow;
  }

  public void setFlow(Float flow) {
    this.flow = flow;
  }

  public TreatmentParameters lungTidalVol(Float lungTidalVol) {
    this.lungTidalVol = lungTidalVol;
    return this;
  }

   /**
   * tidal volume (TV) of lung in ml
   * @return lungTidalVol
  **/
  @Schema(description = "tidal volume (TV) of lung in ml")
  public Float getLungTidalVol() {
    return lungTidalVol;
  }

  public void setLungTidalVol(Float lungTidalVol) {
    this.lungTidalVol = lungTidalVol;
  }

  public TreatmentParameters lungInspiratoryReserveVol(Float lungInspiratoryReserveVol) {
    this.lungInspiratoryReserveVol = lungInspiratoryReserveVol;
    return this;
  }

   /**
   * inspiratory reserve volume (IRV) of lung in ml
   * @return lungInspiratoryReserveVol
  **/
  @Schema(description = "inspiratory reserve volume (IRV) of lung in ml")
  public Float getLungInspiratoryReserveVol() {
    return lungInspiratoryReserveVol;
  }

  public void setLungInspiratoryReserveVol(Float lungInspiratoryReserveVol) {
    this.lungInspiratoryReserveVol = lungInspiratoryReserveVol;
  }

  public TreatmentParameters lungExpiratoryReserveVol(Float lungExpiratoryReserveVol) {
    this.lungExpiratoryReserveVol = lungExpiratoryReserveVol;
    return this;
  }

   /**
   * expiratory reserve volume (ERV) of lung in ml
   * @return lungExpiratoryReserveVol
  **/
  @Schema(description = "expiratory reserve volume (ERV) of lung in ml")
  public Float getLungExpiratoryReserveVol() {
    return lungExpiratoryReserveVol;
  }

  public void setLungExpiratoryReserveVol(Float lungExpiratoryReserveVol) {
    this.lungExpiratoryReserveVol = lungExpiratoryReserveVol;
  }

  public TreatmentParameters lungResidualVol(Float lungResidualVol) {
    this.lungResidualVol = lungResidualVol;
    return this;
  }

   /**
   * residual volume (RV) of lung in ml
   * @return lungResidualVol
  **/
  @Schema(description = "residual volume (RV) of lung in ml")
  public Float getLungResidualVol() {
    return lungResidualVol;
  }

  public void setLungResidualVol(Float lungResidualVol) {
    this.lungResidualVol = lungResidualVol;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TreatmentParameters treatmentParameters = (TreatmentParameters) o;
    return Objects.equals(this.pressure, treatmentParameters.pressure) &&
        Objects.equals(this.frequency, treatmentParameters.frequency) &&
        Objects.equals(this.flow, treatmentParameters.flow) &&
        Objects.equals(this.lungTidalVol, treatmentParameters.lungTidalVol) &&
        Objects.equals(this.lungInspiratoryReserveVol, treatmentParameters.lungInspiratoryReserveVol) &&
        Objects.equals(this.lungExpiratoryReserveVol, treatmentParameters.lungExpiratoryReserveVol) &&
        Objects.equals(this.lungResidualVol, treatmentParameters.lungResidualVol);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pressure, frequency, flow, lungTidalVol, lungInspiratoryReserveVol, lungExpiratoryReserveVol, lungResidualVol);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TreatmentParameters {\n");
    
    sb.append("    pressure: ").append(toIndentedString(pressure)).append("\n");
    sb.append("    frequency: ").append(toIndentedString(frequency)).append("\n");
    sb.append("    flow: ").append(toIndentedString(flow)).append("\n");
    sb.append("    lungTidalVol: ").append(toIndentedString(lungTidalVol)).append("\n");
    sb.append("    lungInspiratoryReserveVol: ").append(toIndentedString(lungInspiratoryReserveVol)).append("\n");
    sb.append("    lungExpiratoryReserveVol: ").append(toIndentedString(lungExpiratoryReserveVol)).append("\n");
    sb.append("    lungResidualVol: ").append(toIndentedString(lungResidualVol)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
