/*
 * Emergency RAVE controller
 * REST API for the Emergency RAVE controller. See https://emergency-rave.gitlab.io/about-erave/ for more information.
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.client.model.DeviceHealthIssues;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * device health information
 */
@Schema(description = "device health information")
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaClientCodegen", date = "2020-04-18T14:46:04.929Z[GMT]")
public class DeviceHealth {
  @SerializedName("status")
  private String status = null;

  @SerializedName("issues")
  private List<DeviceHealthIssues> issues = null;

  public DeviceHealth status(String status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  @Schema(description = "")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public DeviceHealth issues(List<DeviceHealthIssues> issues) {
    this.issues = issues;
    return this;
  }

  public DeviceHealth addIssuesItem(DeviceHealthIssues issuesItem) {
    if (this.issues == null) {
      this.issues = new ArrayList<DeviceHealthIssues>();
    }
    this.issues.add(issuesItem);
    return this;
  }

   /**
   * Get issues
   * @return issues
  **/
  @Schema(description = "")
  public List<DeviceHealthIssues> getIssues() {
    return issues;
  }

  public void setIssues(List<DeviceHealthIssues> issues) {
    this.issues = issues;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeviceHealth deviceHealth = (DeviceHealth) o;
    return Objects.equals(this.status, deviceHealth.status) &&
        Objects.equals(this.issues, deviceHealth.issues);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, issues);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DeviceHealth {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    issues: ").append(toIndentedString(issues)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}
