# SensorReadings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **Integer** |  |  [optional]
**pressureMixChamber** | **Integer** |  |  [optional]
**airFlow** | **Double** |  |  [optional]
**pressureLung** | **Integer** |  |  [optional]
**valveStatusIntervalSec** | **Double** |  |  [optional]
**compressorValveOpenRatio** | **Double** |  |  [optional]
**oxygenValveOpenRatio** | **Double** |  |  [optional]
**inspirationValveOpenRatio** | **Double** |  |  [optional]
**expirationValveOpenRatio** | **Double** |  |  [optional]
