# DefaultApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createcalibrationresult**](DefaultApi.md#createcalibrationresult) | **POST** /calibrationresults | Create a calibrationresult
[**createdeviceHealth**](DefaultApi.md#createdeviceHealth) | **POST** /device-health | Create a device-health
[**createruntimeParameters**](DefaultApi.md#createruntimeParameters) | **POST** /runtime-parameters | Create a runtime-parameters
[**createsensorReadings**](DefaultApi.md#createsensorReadings) | **POST** /sensorreadings | Create a sensorReadings
[**createtreatmentparameters**](DefaultApi.md#createtreatmentparameters) | **POST** /treatmentparameters | Create a treatmentparameters
[**deletecalibrationresult**](DefaultApi.md#deletecalibrationresult) | **DELETE** /calibrationresults/{calibrationresultId} | Delete a calibrationresult
[**deletesensorReadings**](DefaultApi.md#deletesensorReadings) | **DELETE** /sensorreadings/{sensorreadingsId} | Delete a sensorReadings
[**deletetreatmentparameters**](DefaultApi.md#deletetreatmentparameters) | **DELETE** /treatmentparameters/{treatmentparametersId} | Delete a treatmentparameters
[**getcalibrationresult**](DefaultApi.md#getcalibrationresult) | **GET** /calibrationresults/{calibrationresultId} | Get a calibrationresult
[**getcalibrationresults**](DefaultApi.md#getcalibrationresults) | **GET** /calibrationresults | List All calibrationresults
[**getdeviceHealths**](DefaultApi.md#getdeviceHealths) | **GET** /device-health | List All device-healths
[**getruntimeParameters**](DefaultApi.md#getruntimeParameters) | **GET** /runtime-parameters | List All runtime-parameters
[**getsensorReadings**](DefaultApi.md#getsensorReadings) | **GET** /sensorreadings/{sensorreadingsId} | Get a sensorReadings
[**getsensorreadings**](DefaultApi.md#getsensorreadings) | **GET** /sensorreadings | List All sensorreadings
[**gettreatmentparameters**](DefaultApi.md#gettreatmentparameters) | **GET** /treatmentparameters/{treatmentparametersId} | Get a treatmentparameters
[**gettreatmentparameters2**](DefaultApi.md#gettreatmentparameters2) | **GET** /treatmentparameters | List All treatmentparameters
[**updatecalibrationresult**](DefaultApi.md#updatecalibrationresult) | **PUT** /calibrationresults/{calibrationresultId} | Update a calibrationresult
[**updatesensorReadings**](DefaultApi.md#updatesensorReadings) | **PUT** /sensorreadings/{sensorreadingsId} | Update a sensorReadings
[**updatetreatmentparameters**](DefaultApi.md#updatetreatmentparameters) | **PUT** /treatmentparameters/{treatmentparametersId} | Update a treatmentparameters

<a name="createcalibrationresult"></a>
# **createcalibrationresult**
> createcalibrationresult(body)

Create a calibrationresult

Creates a new instance of a &#x60;calibrationresult&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
CalibrationResult body = new CalibrationResult(); // CalibrationResult | A new `calibrationresult` to be created.
try {
    apiInstance.createcalibrationresult(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createcalibrationresult");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CalibrationResult**](CalibrationResult.md)| A new &#x60;calibrationresult&#x60; to be created. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="createdeviceHealth"></a>
# **createdeviceHealth**
> createdeviceHealth(body)

Create a device-health

Creates a new instance of a &#x60;device-health&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
DeviceHealth body = new DeviceHealth(); // DeviceHealth | A new `device-health` to be created.
try {
    apiInstance.createdeviceHealth(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createdeviceHealth");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DeviceHealth**](DeviceHealth.md)| A new &#x60;device-health&#x60; to be created. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="createruntimeParameters"></a>
# **createruntimeParameters**
> createruntimeParameters(body)

Create a runtime-parameters

Creates a new instance of a &#x60;runtime-parameters&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
RuntimeParameters body = new RuntimeParameters(); // RuntimeParameters | A new `runtime-parameters` to be created.
try {
    apiInstance.createruntimeParameters(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createruntimeParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RuntimeParameters**](RuntimeParameters.md)| A new &#x60;runtime-parameters&#x60; to be created. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="createsensorReadings"></a>
# **createsensorReadings**
> createsensorReadings(body)

Create a sensorReadings

Creates a new instance of a &#x60;sensorReadings&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
SensorReadings body = new SensorReadings(); // SensorReadings | A new `sensorReadings` to be created.
try {
    apiInstance.createsensorReadings(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createsensorReadings");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SensorReadings**](SensorReadings.md)| A new &#x60;sensorReadings&#x60; to be created. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="createtreatmentparameters"></a>
# **createtreatmentparameters**
> createtreatmentparameters(body)

Create a treatmentparameters

Creates a new instance of a &#x60;treatmentparameters&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TreatmentParameters body = new TreatmentParameters(); // TreatmentParameters | A new `treatmentparameters` to be created.
try {
    apiInstance.createtreatmentparameters(body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createtreatmentparameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TreatmentParameters**](TreatmentParameters.md)| A new &#x60;treatmentparameters&#x60; to be created. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="deletecalibrationresult"></a>
# **deletecalibrationresult**
> deletecalibrationresult(calibrationresultId)

Delete a calibrationresult

Deletes an existing &#x60;calibrationresult&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String calibrationresultId = "calibrationresultId_example"; // String | A unique identifier for a `calibrationresult`.
try {
    apiInstance.deletecalibrationresult(calibrationresultId);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deletecalibrationresult");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calibrationresultId** | **String**| A unique identifier for a &#x60;calibrationresult&#x60;. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deletesensorReadings"></a>
# **deletesensorReadings**
> deletesensorReadings(sensorreadingsId)

Delete a sensorReadings

Deletes an existing &#x60;sensorReadings&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String sensorreadingsId = "sensorreadingsId_example"; // String | A unique identifier for a `sensorReadings`.
try {
    apiInstance.deletesensorReadings(sensorreadingsId);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deletesensorReadings");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sensorreadingsId** | **String**| A unique identifier for a &#x60;sensorReadings&#x60;. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deletetreatmentparameters"></a>
# **deletetreatmentparameters**
> deletetreatmentparameters(treatmentparametersId)

Delete a treatmentparameters

Deletes an existing &#x60;treatmentparameters&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String treatmentparametersId = "treatmentparametersId_example"; // String | A unique identifier for a `treatmentparameters`.
try {
    apiInstance.deletetreatmentparameters(treatmentparametersId);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deletetreatmentparameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **treatmentparametersId** | **String**| A unique identifier for a &#x60;treatmentparameters&#x60;. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getcalibrationresult"></a>
# **getcalibrationresult**
> CalibrationResult getcalibrationresult(calibrationresultId)

Get a calibrationresult

Gets the details of a single instance of a &#x60;calibrationresult&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String calibrationresultId = "calibrationresultId_example"; // String | A unique identifier for a `calibrationresult`.
try {
    CalibrationResult result = apiInstance.getcalibrationresult(calibrationresultId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getcalibrationresult");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **calibrationresultId** | **String**| A unique identifier for a &#x60;calibrationresult&#x60;. |

### Return type

[**CalibrationResult**](CalibrationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getcalibrationresults"></a>
# **getcalibrationresults**
> List&lt;CalibrationResult&gt; getcalibrationresults()

List All calibrationresults

Gets a list of all &#x60;calibrationresult&#x60; entities.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<CalibrationResult> result = apiInstance.getcalibrationresults();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getcalibrationresults");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;CalibrationResult&gt;**](CalibrationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getdeviceHealths"></a>
# **getdeviceHealths**
> DeviceHealth getdeviceHealths()

List All device-healths

Gets a list of all &#x60;device-health&#x60; entities.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    DeviceHealth result = apiInstance.getdeviceHealths();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getdeviceHealths");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**DeviceHealth**](DeviceHealth.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getruntimeParameters"></a>
# **getruntimeParameters**
> List&lt;RuntimeParameters&gt; getruntimeParameters()

List All runtime-parameters

Gets a list of all &#x60;runtime-parameters&#x60; entities.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<RuntimeParameters> result = apiInstance.getruntimeParameters();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getruntimeParameters");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;RuntimeParameters&gt;**](RuntimeParameters.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getsensorReadings"></a>
# **getsensorReadings**
> SensorReadings getsensorReadings(sensorreadingsId)

Get a sensorReadings

Gets the details of a single instance of a &#x60;sensorReadings&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String sensorreadingsId = "sensorreadingsId_example"; // String | A unique identifier for a `sensorReadings`.
try {
    SensorReadings result = apiInstance.getsensorReadings(sensorreadingsId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getsensorReadings");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sensorreadingsId** | **String**| A unique identifier for a &#x60;sensorReadings&#x60;. |

### Return type

[**SensorReadings**](SensorReadings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getsensorreadings"></a>
# **getsensorreadings**
> List&lt;SensorReadings&gt; getsensorreadings()

List All sensorreadings

Gets a list of all &#x60;sensorReadings&#x60; entities.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<SensorReadings> result = apiInstance.getsensorreadings();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getsensorreadings");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;SensorReadings&gt;**](SensorReadings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="gettreatmentparameters"></a>
# **gettreatmentparameters**
> TreatmentParameters gettreatmentparameters(treatmentparametersId)

Get a treatmentparameters

Gets the details of a single instance of a &#x60;treatmentparameters&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String treatmentparametersId = "treatmentparametersId_example"; // String | A unique identifier for a `treatmentparameters`.
try {
    TreatmentParameters result = apiInstance.gettreatmentparameters(treatmentparametersId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#gettreatmentparameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **treatmentparametersId** | **String**| A unique identifier for a &#x60;treatmentparameters&#x60;. |

### Return type

[**TreatmentParameters**](TreatmentParameters.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="gettreatmentparameters2"></a>
# **gettreatmentparameters2**
> List&lt;TreatmentParameters&gt; gettreatmentparameters2()

List All treatmentparameters

Gets a list of all &#x60;treatmentparameters&#x60; entities.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<TreatmentParameters> result = apiInstance.gettreatmentparameters2();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#gettreatmentparameters2");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TreatmentParameters&gt;**](TreatmentParameters.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updatecalibrationresult"></a>
# **updatecalibrationresult**
> updatecalibrationresult(body, calibrationresultId)

Update a calibrationresult

Updates an existing &#x60;calibrationresult&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
CalibrationResult body = new CalibrationResult(); // CalibrationResult | Updated `calibrationresult` information.
String calibrationresultId = "calibrationresultId_example"; // String | A unique identifier for a `calibrationresult`.
try {
    apiInstance.updatecalibrationresult(body, calibrationresultId);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updatecalibrationresult");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CalibrationResult**](CalibrationResult.md)| Updated &#x60;calibrationresult&#x60; information. |
 **calibrationresultId** | **String**| A unique identifier for a &#x60;calibrationresult&#x60;. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="updatesensorReadings"></a>
# **updatesensorReadings**
> updatesensorReadings(body, sensorreadingsId)

Update a sensorReadings

Updates an existing &#x60;sensorReadings&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
SensorReadings body = new SensorReadings(); // SensorReadings | Updated `sensorReadings` information.
String sensorreadingsId = "sensorreadingsId_example"; // String | A unique identifier for a `sensorReadings`.
try {
    apiInstance.updatesensorReadings(body, sensorreadingsId);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updatesensorReadings");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SensorReadings**](SensorReadings.md)| Updated &#x60;sensorReadings&#x60; information. |
 **sensorreadingsId** | **String**| A unique identifier for a &#x60;sensorReadings&#x60;. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="updatetreatmentparameters"></a>
# **updatetreatmentparameters**
> updatetreatmentparameters(body, treatmentparametersId)

Update a treatmentparameters

Updates an existing &#x60;treatmentparameters&#x60;.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TreatmentParameters body = new TreatmentParameters(); // TreatmentParameters | Updated `treatmentparameters` information.
String treatmentparametersId = "treatmentparametersId_example"; // String | A unique identifier for a `treatmentparameters`.
try {
    apiInstance.updatetreatmentparameters(body, treatmentparametersId);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updatetreatmentparameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TreatmentParameters**](TreatmentParameters.md)| Updated &#x60;treatmentparameters&#x60; information. |
 **treatmentparametersId** | **String**| A unique identifier for a &#x60;treatmentparameters&#x60;. |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

