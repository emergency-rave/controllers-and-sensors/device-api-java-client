# RuntimeParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**oxygenPercent** | **Integer** | percentage of oxygen allowed to flow inside the tank, when oxygen and air are mixed. Allowed values: 0-100 |  [optional]
**respirationsPerMinute** | **Integer** | frequency at which the patient breaths, in breathing cycles per minute. Allowed values: 6-17 |  [optional]
**tankMaxPressure** | **Integer** | Upper end of the pressure range in the tank. If the pressure is higher than the limit, the compressor turns off.  Allowed values: 0-14500 |  [optional]
**tankMinPressure** | **Integer** | Lower end of pressure range in the tank. If the pressure is lower than the limit, the compressor turns on to refill the tank. Allowed values: 0-14500 |  [optional]
**lungsMinPressure** | **Integer** | Lower end of lung pressure range If the pressure is lower than the limit, the inspiration valve turns on after each period passes. Allowed values: 0-1600 |  [optional]
**lungsMaxPressure** | **Integer** | Upper end of lung pressure range. If the pressure is higher than the limit, the inspiration valve turns off after each period passes. Allowed values: 0-1600 |  [optional]
**lungsCriticalPressure** | **Integer** | Warning level for pressure in the lungs. Allowed values: 0-1600 |  [optional]
**operationsMode** | **String** | Operational Mode, one of \&quot;pressure\&quot;, \&quot;time\&quot;, \&quot;interactive\&quot;.  |  [optional]
**inspirationTimePercent** | **Integer** | percentage of time that inspiration takes relative to the length of the  breathing period. Range: 25-75 |  [optional]
**expirationTimePercent** | **Integer** | percentage of time that expiration takes relative to the length of the  breathing period. Allowed range: 25-75 |  [optional]
**pressureUnit** | **String** | One of \&quot;mpsi\&quot;, \&quot;pa\&quot;, \&quot;cmh2o\&quot;, \&quot;mmhg\&quot;, \&quot;mbar\&quot; |  [optional]
**temperatureUnit** | **String** | One of \&quot;celsius\&quot;, \&quot;farenheit\&quot; |  [optional]
