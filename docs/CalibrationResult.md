# CalibrationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**venturiSystemConst** | **Integer** |  |  [optional]
**compressorMbarMax** | **Integer** |  |  [optional]
**flowMlSecMax** | **Integer** |  |  [optional]
