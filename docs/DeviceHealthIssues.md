# DeviceHealthIssues

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **Integer** |  |  [optional]
**message** | **String** |  |  [optional]
**count** | **Integer** |  |  [optional]
**severety** | **String** |  |  [optional]
