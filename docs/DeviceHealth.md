# DeviceHealth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  |  [optional]
**issues** | [**List&lt;DeviceHealthIssues&gt;**](DeviceHealthIssues.md) |  |  [optional]
