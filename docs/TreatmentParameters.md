# TreatmentParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pressure** | **Float** | required lung pressure in mbar |  [optional]
**frequency** | **Float** | breath cycles per second |  [optional]
**flow** | **Float** | moved gas volume per breath cycle in ml |  [optional]
**lungTidalVol** | **Float** | tidal volume (TV) of lung in ml |  [optional]
**lungInspiratoryReserveVol** | **Float** | inspiratory reserve volume (IRV) of lung in ml |  [optional]
**lungExpiratoryReserveVol** | **Float** | expiratory reserve volume (ERV) of lung in ml |  [optional]
**lungResidualVol** | **Float** | residual volume (RV) of lung in ml |  [optional]
